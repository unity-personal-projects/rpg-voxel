Jeu vidéo réalisé dans le cadre d'un exercice d'école pour l'apprentissage d'Unity 3D.

C'est un jeu de chasse de monstre à l troisième personne avec un personnage tiré du site mixamo.com et animé par l'asset TPC Engine.

Les contrôles : Z, Q, S, D + mouvement souris pour le déplacement de cette dernière. Clic gauche pour l'attaque avec le joueur

TODO :

    Ajouter un design graphique sur les monstres afin de les rendre plus vivants.
    Modifier le système de calcul de navigation pour qu'il s'adapte à la modification du terrain.