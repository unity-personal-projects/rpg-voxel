// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "MonsterSurface"
{
	Properties
	{
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		_Speed("Speed", Float) = 1
		_Vector0("Vector 0", Vector) = (5,5,0,0)
		_Teleport("Teleport", Range( -20 , 20)) = 1
		_Range("Range", Range( -10 , 10)) = 1
		[HDR]_GlowColor("Glow Color", Color) = (0.7490196,0.1176471,0.003921569,0)
		_Tint("Tint", Color) = (0,0,0,0)
		_AmbiantOcclusion("Ambiant Occlusion", 2D) = "white" {}
		_Albedo("Albedo", 2D) = "white" {}
		_Normal("Normal", 2D) = "bump" {}
		_Metallic("Metallic", Range( 0 , 1)) = 0
		_Smoothness("Smoothness", Range( 0 , 1)) = 0
		_VertOffsetStrengh("VertOffset Strengh", Float) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows vertex:vertexDataFunc 
		struct Input
		{
			float2 uv_texcoord;
			float3 worldPos;
		};

		uniform float _Teleport;
		uniform float _Range;
		uniform float _VertOffsetStrengh;
		uniform float2 _Vector0;
		uniform float _Speed;
		uniform sampler2D _Normal;
		uniform float4 _Normal_ST;
		uniform sampler2D _Albedo;
		uniform float4 _Albedo_ST;
		uniform sampler2D _AmbiantOcclusion;
		uniform float4 _AmbiantOcclusion_ST;
		uniform float4 _Tint;
		uniform float4 _GlowColor;
		uniform float _Metallic;
		uniform float _Smoothness;
		uniform float _Cutoff = 0.5;


		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float3 ase_vertex3Pos = v.vertex.xyz;
			float4 transform19 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float gradientY17 = saturate( ( ( transform19.y + _Teleport ) / _Range ) );
			float mulTime7 = _Time.y * _Speed;
			float2 panner6 = ( mulTime7 * float2( 0,-1 ) + float2( 0,0 ));
			float2 uv_TexCoord2 = v.texcoord.xy * _Vector0 + panner6;
			float simplePerlin2D3 = snoise( uv_TexCoord2 );
			float Noise11 = ( simplePerlin2D3 + 1.0 );
			float3 VertOffset74 = ( ( ( ase_vertex3Pos * gradientY17 ) * _VertOffsetStrengh ) * Noise11 );
			v.vertex.xyz += VertOffset74;
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_Normal = i.uv_texcoord * _Normal_ST.xy + _Normal_ST.zw;
			float3 NormalMap66 = UnpackNormal( tex2D( _Normal, uv_Normal ) );
			o.Normal = NormalMap66;
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float2 uv_AmbiantOcclusion = i.uv_texcoord * _AmbiantOcclusion_ST.xy + _AmbiantOcclusion_ST.zw;
			float4 Albedo63 = ( tex2D( _Albedo, uv_Albedo ) * tex2D( _AmbiantOcclusion, uv_AmbiantOcclusion ) * _Tint );
			o.Albedo = Albedo63.rgb;
			float mulTime7 = _Time.y * _Speed;
			float2 panner6 = ( mulTime7 * float2( 0,-1 ) + float2( 0,0 ));
			float2 uv_TexCoord2 = i.uv_texcoord * _Vector0 + panner6;
			float simplePerlin2D3 = snoise( uv_TexCoord2 );
			float Noise11 = ( simplePerlin2D3 + 1.0 );
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			float4 transform19 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float gradientY17 = saturate( ( ( transform19.y + _Teleport ) / _Range ) );
			float4 Emission54 = ( ( Noise11 * gradientY17 ) * _GlowColor );
			o.Emission = Emission54.rgb;
			o.Metallic = _Metallic;
			o.Smoothness = _Smoothness;
			o.Alpha = 1;
			float temp_output_44_0 = ( gradientY17 * 1.0 );
			float opacityMask24 = ( ( ( ( 1.0 - gradientY17 ) * Noise11 ) - temp_output_44_0 ) + ( 1.0 - temp_output_44_0 ) );
			clip( opacityMask24 - _Cutoff );
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16700
561;1;1352;1000;2384.669;-512.9409;1;True;False
Node;AmplifyShaderEditor.CommentaryNode;13;-1993.669,41.79939;Float;False;1842.955;721;Noise;9;8;7;5;6;9;3;10;11;2;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;42;-2018.338,828.9636;Float;False;1729.46;694.2577;Gradient Y;8;14;16;19;15;37;41;17;38;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;8;-1943.669,493.7994;Float;False;Property;_Speed;Speed;1;0;Create;True;0;0;False;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;14;-1942.496,911.7781;Float;True;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ObjectToWorldTransfNode;19;-1697.932,878.9637;Float;False;1;0;FLOAT4;0,0,0,1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;16;-1968.338,1117.284;Float;True;Property;_Teleport;Teleport;3;0;Create;True;0;0;False;0;1;-1;-20;20;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;7;-1739.669,509.7994;Float;True;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;5;-1559.669,91.79939;Float;True;Property;_Vector0;Vector 0;2;0;Create;True;0;0;False;0;5,5;55,55;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.PannerNode;6;-1513.669,439.7994;Float;True;3;0;FLOAT2;0,0;False;2;FLOAT2;0,-1;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;15;-1409.88,1033.514;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;38;-1396.507,1265.221;Float;True;Property;_Range;Range;4;0;Create;True;0;0;False;0;1;1;-10;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;37;-1039.865,1075.362;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;2;-1231.494,173.9159;Float;True;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.NoiseGeneratorNode;3;-905.6691,119.7994;Float;True;Simplex2D;1;0;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;41;-758.5073,1053.221;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;9;-865.4141,397.4162;Float;False;Constant;_Booster;Booster;0;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;10;-663.4142,333.4162;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;17;-531.8794,1048.514;Float;True;gradientY;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;49;-2002.963,1626.908;Float;False;1601.366;794.4095;Opacity Mask;9;18;29;21;44;45;47;48;24;22;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;11;-381.7141,343.408;Float;True;Noise;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;81;-82.9826,2039.661;Float;False;1542.91;634.3652;Vertex Offset;8;72;71;73;78;79;76;80;74;;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;18;-1952.963,1676.908;Float;True;17;gradientY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;71;-20.9826,2089.661;Float;True;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;29;-1638.251,1744.739;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;21;-1644.18,1971.743;Float;False;11;Noise;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;58;-1959.23,2508.544;Float;False;1147.777;649.1577;Emission;6;50;56;52;51;57;54;;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;72;-32.9826,2309.661;Float;True;17;gradientY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;73;249.0174,2171.661;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;78;327.8591,2430.026;Float;False;Property;_VertOffsetStrengh;VertOffset Strengh;12;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;70;-34.57882,1191.959;Float;False;1794.06;693;Base Stuff;7;59;62;60;61;63;66;65;;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;51;-1888.62,2765.483;Float;True;17;gradientY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;44;-1706.159,2108.739;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;50;-1909.23,2558.544;Float;True;11;Noise;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;22;-1447.33,1712.066;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;76;598.9105,2221.043;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;79;595.8591,2444.026;Float;True;11;Noise;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;59;111.4211,1677.959;Float;False;Property;_Tint;Tint;6;0;Create;True;0;0;False;0;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;62;17.42117,1241.959;Float;True;Property;_Albedo;Albedo;8;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;60;15.42117,1449.959;Float;True;Property;_AmbiantOcclusion;Ambiant Occlusion;7;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;56;-1617.442,2950.702;Float;False;Property;_GlowColor;Glow Color;5;1;[HDR];Create;True;0;0;False;0;0.7490196,0.1176471,0.003921569,0;1,0.157989,0.004716992,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleSubtractOpNode;45;-1215.491,1874.812;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;47;-1226.791,2168.317;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;52;-1614.364,2688.098;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;80;845.8591,2300.026;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;57;-1318.616,2809.581;Float;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;65;1118.647,1265.174;Float;True;Property;_Normal;Normal;9;0;Create;True;0;0;False;0;None;None;True;0;False;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;61;445.421,1323.959;Float;True;3;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;48;-979.7489,2067.333;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;74;1216.927,2197.982;Float;True;VertOffset;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;66;1516.481,1312.203;Float;True;NormalMap;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;63;755.421,1309.959;Float;True;Albedo;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;24;-644.5963,2022.153;Float;True;opacityMask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;54;-1054.453,2803.077;Float;True;Emission;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;64;588.3297,46.87228;Float;True;63;Albedo;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;69;252.1196,693.2811;Float;False;Property;_Smoothness;Smoothness;11;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;33;412.4672,796.9801;Float;True;24;opacityMask;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;67;315.1988,229.6338;Float;True;66;NormalMap;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;68;242.1196,615.2811;Float;False;Property;_Metallic;Metallic;10;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;75;522.512,1006.388;Float;False;74;VertOffset;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;55;227.2872,392.67;Float;True;54;Emission;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;35;997.1454,334.8531;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;MonsterSurface;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;True;Transparent;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;19;0;14;0
WireConnection;7;0;8;0
WireConnection;6;1;7;0
WireConnection;15;0;19;2
WireConnection;15;1;16;0
WireConnection;37;0;15;0
WireConnection;37;1;38;0
WireConnection;2;0;5;0
WireConnection;2;1;6;0
WireConnection;3;0;2;0
WireConnection;41;0;37;0
WireConnection;10;0;3;0
WireConnection;10;1;9;0
WireConnection;17;0;41;0
WireConnection;11;0;10;0
WireConnection;29;0;18;0
WireConnection;73;0;71;0
WireConnection;73;1;72;0
WireConnection;44;0;18;0
WireConnection;22;0;29;0
WireConnection;22;1;21;0
WireConnection;76;0;73;0
WireConnection;76;1;78;0
WireConnection;45;0;22;0
WireConnection;45;1;44;0
WireConnection;47;0;44;0
WireConnection;52;0;50;0
WireConnection;52;1;51;0
WireConnection;80;0;76;0
WireConnection;80;1;79;0
WireConnection;57;0;52;0
WireConnection;57;1;56;0
WireConnection;61;0;62;0
WireConnection;61;1;60;0
WireConnection;61;2;59;0
WireConnection;48;0;45;0
WireConnection;48;1;47;0
WireConnection;74;0;80;0
WireConnection;66;0;65;0
WireConnection;63;0;61;0
WireConnection;24;0;48;0
WireConnection;54;0;57;0
WireConnection;35;0;64;0
WireConnection;35;1;67;0
WireConnection;35;2;55;0
WireConnection;35;3;68;0
WireConnection;35;4;69;0
WireConnection;35;10;33;0
WireConnection;35;11;75;0
ASEEND*/
//CHKSM=798D0F8B4F5B8AC53FBCA07C724E8AF0D7052016