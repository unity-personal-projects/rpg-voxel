﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SDD.Events;
using UnityEngine.AI;

public class EnemyManager : Manager<EnemyManager>
{
    [SerializeField]
    GameObject m_EnemyModel;
    List<GameObject> m_Enemies = new List<GameObject>();

    protected override IEnumerator InitCoroutine()
    {
        yield return null;
    }

    private new void Awake()
    {
        EventManager.Instance.AddListener<PlayerHitEnemyWithSword>(OnPlayerHitEnemyWithSword);
        EventManager.Instance.AddListener<WaveStart>(OnWaveStart);
    }

    /// <summary>
    /// Gère quand le joueur touche un ennemi. Parcours tous les monstres pour savoir celui qui est touché (envoie un message pour son de dégâts) et s'il meurt :
    /// Envoie un message de mort
    /// </summary>
    /// <param name="o"></param>
    private void OnPlayerHitEnemyWithSword(PlayerHitEnemyWithSword o)
    {
        if (!m_Enemies.Contains(o.enemy))
        {
            return;
        }
        GameObject toBeRemoved = null;
        // Parcours les enemis
        foreach (GameObject enemy in m_Enemies)
        {
            Monster compMonster = enemy.GetComponent<Monster>();
            // S'il trouve l'enemi touché
            if (GameObject.ReferenceEquals(enemy, o.enemy))
            {
                // Envoie la prise de dégâts pour le son du monstre
                EventManager.Instance.Raise(new EnemyTakingDamage() { enemyTakingDamageGO = enemy });
                // Baise les PV du monstre
                compMonster.LifePoints -= 30;
                if (compMonster.LifePoints <= 0) // Si les PV du monstre sont à 0
                {
                    // Stock le monstre à détruire dans une variable pour le détruire hors de l'itération
                    toBeRemoved = enemy;
                    break;
                }
            }
        }

        if (toBeRemoved) // Si monstre à détruire existe
        {
            // Supression de la liste
            m_Enemies.Remove(toBeRemoved);
            // Monte le score
            GameManager.Instance.Score += 100;
            //Envoie l'event qui sera recueillit par le script Monster.cs (détruit le monstre)
            EventManager.Instance.Raise(new EnemyKilled() { enemyToDestroy = toBeRemoved });
        }
        if (m_Enemies.Count <= 30) // S'il ne reste que 30 monstre alors on termine la vague (c'est fatiguant de se balader dans la carte pendant 1 heure)
        {
            m_Enemies = new List<GameObject>();
            EventManager.Instance.Raise(new WaveEnded());
        }
    }

    /// <summary>
    /// À chaque début de vague : détruit les ennemis actuels et instencie de nouveaux ennemis
    /// </summary>
    /// <param name="o"></param>
    private void OnWaveStart(WaveStart o)
    {
        DestroyAllEnemies();
        InstantiateEnemies(o.amountOfEnemies);
    }

    /// <summary>
    /// Instance une quantitée d'ennemis donnée et les place sur la carte
    /// </summary>
    /// <param name="amountOfEnemies">Quantité d'ennemis</param>
    private void InstantiateEnemies(int amountOfEnemies)
    {
        for (int i = 0; i < amountOfEnemies; i++)
        {
            float coordX = Random.Range(10f, 470f);
            float coordZ = Random.Range(10f, 470f);
            Vector3 position = new Vector3(coordX, 50, coordZ);
            GameObject enemy = Instantiate(m_EnemyModel) as GameObject;
            enemy.GetComponent<NavMeshAgent>().Warp(position);
            enemy.transform.SetParent(gameObject.transform);
        }
        // Ajout des enemis dans une liste de monstre
        // (ceci est fait en dehors de la première itération au cas où on débug avec un enemi déjà posé sur la carte)
        foreach(Monster mon in GameObject.FindObjectsOfType<Monster>())
        {
            m_Enemies.Add(mon.gameObject);
        }
    }

    /// <summary>
    /// Détruit tous les ennemis de la map (quand on retourne au menu)
    /// </summary>
    private void DestroyAllEnemies()
    {
        if (m_Enemies.Count > 0)
        {
            foreach (GameObject enemy in m_Enemies)
            {
                Destroy(enemy);
            }
        }
    }
    
    void Destroy()
    {
        EventManager.Instance.RemoveListener<PlayerHitEnemyWithSword>(OnPlayerHitEnemyWithSword);
        EventManager.Instance.RemoveListener<WaveStart>(OnWaveStart);

    }
}
