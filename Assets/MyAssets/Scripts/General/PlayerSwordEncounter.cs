﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SDD.Events;

public class PlayerSwordEncounter : MonoBehaviour
{
    PlayerControl m_PlayerContrl;

    private void Awake()
    {
        m_PlayerContrl = GameObject.FindObjectOfType<PlayerControl>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Monster>())
        {
            if (other.gameObject.GetComponent<Monster>() && m_PlayerContrl.IsAttacking)
            {
                Debug.Log("Touche");
                EventManager.Instance.Raise(new PlayerHitEnemyWithSword() { enemy = other.gameObject });
            }
        }
    }
}
