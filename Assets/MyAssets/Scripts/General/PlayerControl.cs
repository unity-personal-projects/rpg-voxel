﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SDD.Events;
using UnityEngine.UI;

public class PlayerControl : MonoBehaviour
{
    bool isAttacking = false;
    public bool IsAttacking { get => isAttacking; set => isAttacking = value; }

    [SerializeField] Collider m_SwordCollider;
    Animator m_Animator;
    float m_PlayerLP;
    [SerializeField]
    float m_playerMaxLP = 100f;
    TPCEngine.TPCharacter m_Character;
    TPCEngine.TPFootstepsSoundSystem m_TPCSoundSystem;
    VoxelandControllerMaria m_VoxelandController;
    TPCEngine.StandaloneController m_StandaloneController;
    TPCEngine.TPCamera m_TPCamera;
    Image m_CurrentHPBar;

    AudioSource m_SwordAudioSource;
    [SerializeField]
    AudioClip m_AudioSwordWoosh;
    Coroutine m_Touched;

    private void Awake()
    {
        IsAttacking = false;
        EventManager.Instance.AddListener<EnemyTouchPlayer>(OnAttacked);
		EventManager.Instance.AddListener<PlayButtonClickedEvent>(OnPlayButtonClicked);
    }

    /// <summary>
    /// Définition de toutes les variables stockées en cache
    /// </summary>
    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Character = GetComponent<TPCEngine.TPCharacter>();
        m_TPCSoundSystem = GetComponent<TPCEngine.TPFootstepsSoundSystem>();
        m_VoxelandController = GetComponent<VoxelandControllerMaria>();
        m_StandaloneController = GetComponent<TPCEngine.StandaloneController>();
        m_TPCamera = GetComponentInChildren<TPCEngine.TPCamera>();
        m_SwordAudioSource = GetComponentInChildren<PlayerSwordEncounter>().gameObject.GetComponent<AudioSource>();
    }

    /// <summary>
    /// Appelle la coroutine de prise de dégâts du joueur
    /// </summary>
    /// <param name="e"></param>
    void OnAttacked(EnemyTouchPlayer e)
    {
        if (m_Touched == null)
        {
            m_Touched = StartCoroutine(Touched(e.eDamages));
        }
    }

    /// <summary>
    /// Quand le joueur est touché : baisse les PV (possibilité d'un son de prise de dégâts)
    /// Empèche aussi de prends des dégâts en continu
    /// </summary>
    /// <param name="damages"></param>
    /// <returns></returns>
    IEnumerator Touched(float damages)
    {
        m_PlayerLP -= damages;
        yield return new WaitForSeconds(0.6f);
        m_Touched = null;
    }

    /// <summary>
    /// QUand on démarre le jeu, on mets les PV du joueur à 100
    /// </summary>
    /// <param name="e"></param>
    private void OnPlayButtonClicked(PlayButtonClickedEvent e)
    {
        m_PlayerLP = m_playerMaxLP;
    }

    void Update()
    {
        // Si le jeu n'est pas en mode play alors on désactive les scripts gênants et on rends le curseur visible
        if (!GameManager.Instance.IsPlaying)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            m_TPCamera.enabled = false;
            m_Character.enabled = false;
            m_Animator.enabled = false;
            m_StandaloneController.enabled = false;
            m_VoxelandController.enabled = false;
            return;
        }
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        m_TPCamera.enabled = true;
        m_Character.enabled = true;
        m_Animator.enabled = true;
        m_StandaloneController.enabled = true;
        m_VoxelandController.enabled = true;

        // Check si le bouton de la souris est activé (le joueur attaque attaque).
        // S'il attaque déjà alors il ne peut pas attaque de nouveau tant que l'attaque n'est pas finie
        if (Input.GetMouseButtonDown(0) && !IsAttacking)
        {
            StartCoroutine(Attack());
        }
        // Récupère la barre de HP (impossible de la récupérer avant car menu désactivé)
        if (m_CurrentHPBar == null)
        {
            m_CurrentHPBar = GameObject.FindObjectOfType<CurrentLPBarFinder>().gameObject.GetComponent<Image>();
        }

    }

    private void LateUpdate()
    {
        // Si le jeu n'est pas en mode play alors on stop la fonction ici
        if (!GameManager.Instance.IsPlaying)
        {
            return;
        }
        // Si le joueur n'a plus de PV alors on update la barre de vie et on lance l'événement de mort du joueur
        if (m_PlayerLP <= 0)
        {
            m_CurrentHPBar.fillAmount = 0f;
            EventManager.Instance.Raise(new PlayerDead());
        }
        else // Sinon, on update juste la barre de PV
        {
            float hpPercentage = ((m_PlayerLP * 100f) / m_playerMaxLP) / 100f;
            m_CurrentHPBar.fillAmount = hpPercentage;
        }
    }

    /// <summary>
    /// Coroutine d'attaque du joueur. Joue un son et une animation. Active le collider de l'épée.
    /// </summary>
    /// <returns></returns>
    IEnumerator Attack()
    {
        m_Animator.SetBool("IsAttacking", true);
        m_SwordCollider.enabled = true;

        yield return new WaitForSeconds(.10f);
        IsAttacking = true;
        yield return new WaitForSeconds(.1f);
        m_SwordAudioSource.clip = m_AudioSwordWoosh;
        m_SwordAudioSource.Play();
        yield return new WaitForSeconds(.6f);

        IsAttacking = false;
        m_Animator.SetBool("IsAttacking", false);
        m_SwordCollider.enabled = false;
    }

    private void OnDestroy()
    {
		EventManager.Instance.RemoveListener<PlayButtonClickedEvent>(OnPlayButtonClicked);
        EventManager.Instance.RemoveListener<EnemyTouchPlayer>(OnAttacked);
    }
}
