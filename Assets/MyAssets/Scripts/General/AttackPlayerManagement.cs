﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SDD.Events;

public class AttackPlayerManagement : MonoBehaviour
{
    /// <summary>
    /// Quand l'épée de l'enemi touche le joueur : envoie un event avec 10 pts de dégâts
    /// (peut varier dans le futur suivant une caractéristique de force du monstre qui serait randomizée à sa création)
    /// </summary>
    /// <param name="other">Si le joueur est touché par une épée de monstre</param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<EnemySword>())
        {
            EventManager.Instance.Raise(new EnemyTouchPlayer() { eDamages = 10 });
        }
    }
}
