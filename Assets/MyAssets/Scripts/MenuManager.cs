﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SDD.Events;
using UnityEngine.UI;

public class MenuManager : Manager<MenuManager>
{

    [Header("MenuManager")]

    #region Panels
    [Header("Panels")]
    [SerializeField] GameObject m_PanelMainMenu;
    [SerializeField] GameObject m_PanelInGameMenu;
    [SerializeField] GameObject m_PanelHUD;
    [SerializeField] GameObject m_PanelGameOver;

    List<GameObject> m_AllPanels;
    #endregion

    #region Events' subscription
    public override void SubscribeEvents()
    {
        base.SubscribeEvents();
		EventManager.Instance.AddListener<EnemyKilled>(OnEnemyKilled);
		EventManager.Instance.AddListener<PlayerDead>(OnPlayerDead);
    }

    public override void UnsubscribeEvents()
    {
        base.UnsubscribeEvents();
    }
    #endregion

    #region Manager implementation
    protected override IEnumerator InitCoroutine()
    {
        yield break;
    }
    #endregion

    #region Monobehaviour lifecycle
    protected override void Awake()
    {
        base.Awake();
        RegisterPanels();
    }
    protected override IEnumerator Start()
    {
        m_PanelInGameMenu.SetActive(false);
        m_PanelHUD.SetActive(false);
        m_PanelGameOver.SetActive(false);
        yield return null;
    }

    private void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            EscapeButtonHasBeenClicked();
        }
    }
    #endregion

    #region Panel Methods
    void RegisterPanels()
    {
        m_AllPanels = new List<GameObject>
        {
            m_PanelMainMenu,
            m_PanelInGameMenu,
            m_PanelGameOver
        };
    }

    void OpenPanel(GameObject panel)
    {
        foreach (var item in m_AllPanels)
        {
            if (item) item.SetActive(item == panel);
        }
    }
    #endregion

    #region UI OnClick Events
    public void EscapeButtonHasBeenClicked()
    {
        EventManager.Instance.Raise(new EscapeButtonClickedEvent());
    }

    public void PlayButtonHasBeenClicked()
    {
        EventManager.Instance.Raise(new PlayButtonClickedEvent());
    }

    public void ResumeButtonHasBeenClicked()
    {
        EventManager.Instance.Raise(new ResumeButtonClickedEvent());
    }

    public void MainMenuButtonHasBeenClicked()
    {
        EventManager.Instance.Raise(new MainMenuButtonClickedEvent());
    }

    public void QuitButtonHasBeenClicked()
    {
        EventManager.Instance.Raise(new QuitButtonClickedEvent());
    }

    #endregion

    #region Callbacks to GameManager events
    protected override void GameMenu(GameMenuEvent e)
    {
        OpenPanel(m_PanelMainMenu);
    }

    protected override void GamePlay(GamePlayEvent e)
    {
        OpenPanel(null);
    }

    protected override void GamePause(GamePauseEvent e)
    {
        m_PanelHUD.SetActive(false);
        OpenPanel(m_PanelInGameMenu);
    }

    protected override void GameResume(GameResumeEvent e)
    {
        OpenPanel(null);
    }

    protected override void GameOver(GameOverEvent e)
    {
        OpenPanel(m_PanelGameOver);
    }
    #endregion

    private void OnEnemyKilled(EnemyKilled o)
    {
        Text currentScore = m_PanelHUD.GetComponentInChildren<CurrentScoreFinder>().gameObject.GetComponent<Text>();
        currentScore.text = GameManager.Instance.Score.ToString();
        Text bestScore = m_PanelHUD.GetComponentInChildren<BestScoreFinder>().gameObject.GetComponent<Text>();
        bestScore.text = GameManager.Instance.BestScore.ToString();
    }

    private void OnPlayerDead(PlayerDead e)
    {
        m_PanelMainMenu.SetActive(false);
        m_PanelInGameMenu.SetActive(false);
        m_PanelHUD.SetActive(false);
        m_PanelGameOver.SetActive(true);
    }
}