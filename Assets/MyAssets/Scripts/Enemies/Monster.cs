﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using SDD.Events;
using System;

public class Monster : MonoBehaviour
{
    GameObject m_Player;
    NavMeshAgent m_NavMeshAgent;
    Animator m_Animator;

    [SerializeField]
    AudioClip m_AudioMonsterDie;
    [SerializeField]
    AudioClip m_AudioTakingDamage;
    AudioSource m_AudioSource;

    bool m_AttackPlayerRunning = false;
    Coroutine m_AttackPlayer;
    RaycastHit hit;
    GameObject m_LaserBeam;
    public bool m_isDying = false;
    Coroutine m_ToDestroy;
    List<Material> m_MaterialsShader = new List<Material>();

    [SerializeField] int m_lifePoints = 100;

    float m_CurrentHeight, m_PreviousHeight;

    public int LifePoints { get => m_lifePoints; set => m_lifePoints = value; }

    private void Awake()
    {
        EventManager.Instance.AddListener<EnemyKilled>(OnEnemyKilled);
        EventManager.Instance.AddListener<EnemyTakingDamage>(OnEnemyTakingDamage);
    }

    /// <summary>
    /// Quand l'ennemi prends des dommages
    /// </summary>
    /// <param name="e"></param>
    private void OnEnemyTakingDamage(EnemyTakingDamage e)
    {
        // Si l'ennemi n'est pas déjà entrain de mourrir (son de mort différent de son de prise de dégâts)
        if (m_ToDestroy == null)
        {
            if (GameObject.ReferenceEquals(gameObject, e.enemyTakingDamageGO))
            {
                // Joue le son de prise de dégâts
                m_AudioSource.clip = m_AudioTakingDamage;
                m_AudioSource.Play();
            }
        }
    }

    /// <summary>
    /// Reception de la mort d'un enemie. Si l'ennemi tué est l'ennemi gameObject alors on démarre la coroutine EnemyDie.
    /// </summary>
    /// <param name="e"></param>
    private void OnEnemyKilled(EnemyKilled e)
    {
        if (m_ToDestroy == null)
        {
            if (GameObject.ReferenceEquals(gameObject, e.enemyToDestroy))
            {
                m_ToDestroy = StartCoroutine(EnemyDie());
            }
        }
    }

    /// <summary>
    /// Coroutine de mort du monstre
    /// </summary>
    /// <returns></returns>
    private IEnumerator EnemyDie()
    {
        // Désactive le collider
        GetComponent<Collider>().enabled = false;
        m_isDying = true;
        // Joue le son de mort du monstre
        m_AudioSource.clip = m_AudioMonsterDie;
        m_AudioSource.Play();
        // Update le shader du matériau créé avec Amplify Shader
        for (float i = -2.4f; i < 5f; i = i + 0.1f)
        {
            foreach(Material mat in m_MaterialsShader)
            {
                mat.SetFloat("_Teleport", i);
            }
            yield return 0;
        }
        Destroy(gameObject);
    }

    /// <summary>
    /// Définition de toutes les variables stockées
    /// </summary>
    void Start()
    {
        m_PreviousHeight = m_CurrentHeight = transform.position.y;
        m_Player = GameObject.FindObjectOfType<PlayerControl>().gameObject;
        m_NavMeshAgent = GetComponent<NavMeshAgent>();
        m_Animator = GetComponent<Animator>();
        m_LaserBeam = gameObject.GetComponentInChildren<LaserBeamFinder>().gameObject;
        m_AudioSource = GetComponent<AudioSource>();
        // Récupère tous les renderer du monstre pour la destruction.
        // (Le Shader de destruction (MonsterSurface) a été fait par mes soins grâce à l'asset Amplify Shader (réseau nodal))
        m_MaterialsShader.Add(gameObject.GetComponent<Renderer>().material);
        foreach (Renderer rend in gameObject.GetComponentsInChildren<Renderer>())
        {
            m_MaterialsShader.Add(rend.material);
        }
    }
    
    void Update()
    {
        // Si le jeu est en mode play
        if (!GameManager.Instance.IsPlaying)
        {
            return;
        }
        // Si le monstre meurt alors on bloque son déplacement
        if (m_isDying)
        {
            m_NavMeshAgent.SetDestination(transform.position);
            return;
        }
        // S'occupe du navmesh s'il y a une erreur
        CheckNavMeshManagement();

        // s'occupe du déplacement du monstre
        PlayerOnGazeManagement();
    }

    /// <summary>
    /// Active le navigation mesh lors de l'apparition des monstres (sinon ils ne se déplacent pas car ils apparaissent trop loin de la map)
    /// </summary>
    private void CheckNavMeshManagement()
    {
        m_CurrentHeight = transform.position.y;
        if (m_CurrentHeight + 0.5f < m_PreviousHeight)
        {
            m_PreviousHeight = m_CurrentHeight;
            m_NavMeshAgent.Warp(transform.position);
        }
    }

    /// <summary>
    /// Gère si le monstre voit le joueur alors il se déplace à lui
    /// </summary>
    private void PlayerOnGazeManagement()
    {
        if (CheckPlayerOnGaze())
        {
            if (Vector3.Distance(transform.position, m_Player.transform.position) < 2f)
            {
                if (!m_AttackPlayerRunning)
                {
                    m_AttackPlayer = StartCoroutine(AttackPlayer(hit.transform.gameObject));
                }
                m_NavMeshAgent.isStopped = true;
            }
            else
            {
                if (m_AttackPlayerRunning)
                {
                    StopCoroutine(m_AttackPlayer);
                    m_AttackPlayer = null;
                    m_Animator.Play("Idle");
                }
                m_AttackPlayerRunning = false;
                Debug.DrawRay(transform.position, m_Player.GetComponentInChildren<RaycastHitMaria>().transform.position - transform.position, Color.green);
                m_NavMeshAgent.destination = hit.transform.position;
                m_NavMeshAgent.isStopped = false;
            }
        }
    }

    /// <summary>
    /// Check si le monstre peut voir le joueur
    /// </summary>
    /// <returns>Booléen si le monstre voit le joueur</returns>
    private bool CheckPlayerOnGaze()
    {
        GameObject raycastHitTarget = m_Player.GetComponentInChildren<RaycastHitMaria>().gameObject;
        Vector3 targetDir = raycastHitTarget.transform.position - transform.position;
        Physics.Raycast(transform.position, targetDir, out hit, 100);
        if (hit.transform != null)
        {
            float angleToPlayer = (Vector3.Angle(targetDir, transform.forward));
            bool playerOnGaze = angleToPlayer >= -80 && angleToPlayer <= 80;
            bool distanceFromPlayer = Vector3.Distance(transform.position, m_Player.transform.position) < 32f;
            bool playerAround = Vector3.Distance(transform.position, m_Player.transform.position) < 9f;

            return hit.transform.GetComponent<PlayerControl>()
                && ((playerOnGaze
                && distanceFromPlayer)
                || playerAround);
        }
        return false;
    }

    /// <summary>
    /// Coroutine permettant au monstre d'attaquer le joueur
    /// </summary>
    /// <param name="player">Le joueur ciblé</param>
    /// <returns></returns>
    IEnumerator AttackPlayer(GameObject player)
    {
        m_AttackPlayerRunning = true;
        while (true)
        {
            m_Animator.Play("AttackPlayer");
            yield return new WaitForSeconds(1f);
            m_Animator.Play("Idle");
            yield return new WaitForSeconds(1f);
        }
    }

    private void OnDestroy()
    {
        EventManager.Instance.RemoveListener<EnemyKilled>(OnEnemyKilled);
        EventManager.Instance.RemoveListener<EnemyTakingDamage>(OnEnemyTakingDamage);
    }
}
