﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SDD.Events;

#region GameManager Events
public class GameMenuEvent : SDD.Events.Event
{
}
public class GamePlayEvent : SDD.Events.Event
{
}
public class GamePauseEvent : SDD.Events.Event
{
}
public class GameResumeEvent : SDD.Events.Event
{
}
public class GameOverEvent : SDD.Events.Event
{
}
public class GameVictoryEvent : SDD.Events.Event
{
}

public class GameStatisticsChangedEvent : SDD.Events.Event
{
	public float eBestScore { get; set; }
	public float eScore { get; set; }
	public int eNLives { get; set; }
}
#endregion

#region MenuManager Events
public class EscapeButtonClickedEvent : SDD.Events.Event
{
}
public class PlayButtonClickedEvent : SDD.Events.Event
{
}
public class ResumeButtonClickedEvent : SDD.Events.Event
{
}
public class MainMenuButtonClickedEvent : SDD.Events.Event
{
}

public class QuitButtonClickedEvent : SDD.Events.Event
{ }
#endregion

#region Score Event
public class ScoreItemEvent : SDD.Events.Event
{
	public float eScore;
}
#endregion


#region Enemy Interactions

/// <summary>
/// Event trigger quand l'enemie touche le joueur
/// </summary>
public class EnemyTouchPlayer : SDD.Events.Event
{
    public float eDamages;
}

/// <summary>
/// Event quand le joueur touche l'ennemi avec son épée
/// </summary>
public class PlayerHitEnemyWithSword : SDD.Events.Event
{
    public GameObject enemy;
}

/// <summary>
/// Event quand une vague démarre
/// </summary>
public class WaveStart : SDD.Events.Event
{
    public int amountOfEnemies;
}

/// <summary>
/// Event quand une vague se termine
/// </summary>
public class WaveEnded : SDD.Events.Event
{ }

/// <summary>
/// Quand un enemi est tué
/// </summary>
public class EnemyKilled : SDD.Events.Event
{
    public GameObject enemyToDestroy;
}

/// <summary>
/// Quand un enemi prends des dommages
/// </summary>
public class EnemyTakingDamage : SDD.Events.Event
{
    public GameObject enemyTakingDamageGO;
}


#endregion


#region Player interactions

/// <summary>
/// Quand le joueur meurt
/// </summary>
public class PlayerDead : SDD.Events.Event
{ }

#endregion